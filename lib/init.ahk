SetTimer(initAll, -400)

initAll()
{
    Suspend(true)
    loadValue := IniRead("CapsLocksettings.ini", "GlobalSettings", "loadani", 1)
    if(loadValue)
        Animation.loadingAnimation()
    keysInit()
    Suspend(false)
    if(loadValue)
        Animation.closeAnimation()
    return
}

class Animation {

    ;#region
    static LoadingChar := ["_('<------"
        , " _('=-----"
        , " _('<-----"
        , "  _('=----"
        , "  _('<----"
        , "   _('=---"
        , "   _('<---"
        , "    _('=--"
        , "    _('<--"
        , "     _('=-"
        , "     _('<-"
        , "      _(*="
        , "------_(*="
        , "------_(^<"
        , "------_(^<"
        , "------ |  "
        , "------>')_"
        , "-----=')_ "
        , "----->')_ "
        , "----=')_  "
        , "---->')_  "
        , "---=')_   "
        , "--->')_   "
        , "--=')_    "
        , "-->')_    "
        , "-=')_     "
        , "->')_     "
        , "=*)_      "
        , "=*)_------"
        , ">^)_------"
        , ">^)_------"
        , "  | ------"]
    ;#endregion

    static MyGui := Gui()
    static loadingAnimation()
    {
        this.MyGui.Opt("-Caption +Owner  +AlwaysOnTop")
        this.MyGui.SetFont("C0x555555 s12", "Lucida Console")
        this.MyGui.SetFont("C0x555555 s12", "Fixedsys")
        this.MyGui.SetFont("C0x555555 s12", "Courier New")
        this.MyGui.SetFont("C0x555555 s12", "Consolas")
        this.LoadingText := this.MyGui.Add("Text", "H20 W100 Center", this.LoadingChar[1])
        this.MyGui.BackColor := "ffffff"
        WinSetTransparent("250", this.MyGui)
        this.MyGui.Show("Center NA")
        this.timer := ObjBindMethod(this, "changeLoadingChar")
        SetTimer(this.timer, 250, 777)
        return
    }

    static changeLoadingChar()
    {
        loadingCharMaxIndex := this.LoadingChar.Length
        static charIndex := 1
        charIndex := Mod(charIndex, loadingCharMaxIndex) + 1
        ; MsgBox(charIndex, loadingCharMaxIndex)
        ControlSetText(this.LoadingChar[charIndex], this.LoadingText)
    }

    static closeAnimation() {
        SetTimer(this.timer, 0)
        this.MyGui.Destroy()
        return
    }

}