﻿;如果没有在set.ini设置按键的话，就按这里的默认设置执行
keysInit()
{
    global
        ; keyset_press_caps := "keyFunc_toggleCapsLock"
        keyset_press_caps := "keyFunc_esc"
    ;move
        keyset_caps_h := "keyFunc_moveLeft"
        keyset_caps_j := "keyFunc_moveDown"
        keyset_caps_k := "keyFunc_moveUp"
        keyset_caps_l := "keyFunc_moveRight"
        keyset_caps_f := "keyFunc_moveWordLeft"
        keyset_caps_g := "keyFunc_moveWordRight"
        keyset_caps_p := "keyFunc_home"
        keyset_caps_semicolon := "keyFunc_end"
    ;select
        keyset_caps_u := "keyFunc_selectHome"
        keyset_caps_o := "keyFunc_end"
        keyset_caps_comma:="keyFunc_selectCurrentWord"
    ;switch tab
        keyset_caps_n := "keyFunc_tabNext"
        keyset_caps_b := "keyFunc_tabPrve"

    ;sys key replace
        keyset_caps_c := "keyFunc_copy"
        keyset_caps_v := "keyFunc_paste"          

    ;Don't use now
        keyset_caps_i := "keyFunc_doNothing"
        keyset_caps_m := "keyFunc_doNothing"
        keyset_caps_y := "keyFunc_doNothing"
        keyset_caps_t := "keyFunc_doNothing"
        keyset_caps_q := "keyFunc_doNothing"
        keyset_caps_w := "keyFunc_doNothing"
        keyset_caps_e := "keyFunc_doNothing"
        keyset_caps_r := "keyFunc_doNothing"
        keyset_caps_z := "keyFunc_doNothing"
        keyset_caps_x := "keyFunc_doNothing"
        keyset_caps_a := "keyFunc_doNothing"
        keyset_caps_d := "keyFunc_doNothing"
        keyset_caps_s := "keyFunc_doNothing"
        keyset_caps_1 := "keyFunc_doNothing"
        keyset_caps_2 := "keyFunc_doNothing"
        keyset_caps_3 := "keyFunc_doNothing"
        keyset_caps_4 := "keyFunc_doNothing"
        keyset_caps_5 := "keyFunc_doNothing"
        keyset_caps_6 := "keyFunc_doNothing"
        keyset_caps_7 := "keyFunc_doNothing"
        keyset_caps_8 := "keyFunc_doNothing"
        keyset_caps_9 := "keyFunc_doNothing"
        keyset_caps_0 := "keyFunc_doNothing"
        keyset_caps_f1 := "keyFunc_doNothing"
        keyset_caps_f2 := "keyFunc_doNothing"
        keyset_caps_f3 := "keyFunc_doNothing"
        keyset_caps_f4 := "keyFunc_doNothing"
        keyset_caps_f5 := "keyFunc_doNothing"
        keyset_caps_f6 := "keyFunc_doNothing"
        keyset_caps_f7 := "keyFunc_doNothing"
        keyset_caps_f8 := "keyFunc_doNothing"
        keyset_caps_f9 := "keyFunc_doNothing"
        keyset_caps_f10 := "keyFunc_doNothing"
        keyset_caps_f11 := "keyFunc_doNothing"
        keyset_caps_f12 := "keyFunc_doNothing"
}
