  class keyset_funcitons {
      ; keys functions start-------------
      ; 所有按键对应功能都放在这，为防止从set.ini通过按键设置调用到非按键功能函数，
      ; 规定函数以"static keyFunc_"开头
      static keyFunc_doNothing() {
          return
      }

      static keyFunc_copy() {
          SendInput "^c"
          return
      }

      static keyFunc_paste()
      {
          SendInput "^v"
          return
      }

      static keyFunc_copyline()
      {
        SendInput "{Home}"
        SendInput "+{End}"
        SendInput "^c"
        SendInput "{End}"
        SendInput "{Enter}"
        SendInput "^v"
        return
      }

      static keyFunc_send(p) {
          SendInput p
          return
      }

      static keyFunc_Run(p) {
          Run p
          return
      }

      static keyFunc_toggleCapsLock() {
          SetCapsLockState GetKeyState("CapsLock", "T") ? "Off" : "On"
                  return
      }

      static keyFunc_moveLeft() {
          SendInput "{Left}"
          return
      }

      static keyFunc_moveRight() {
          SendInput "{Right}"
          Return
      }

      static keyFunc_moveUp() {
          SendInput "{Up}"
          Return
      }

      static keyFunc_moveDown() {
          SendInput "{Down}"
          Return
      }

      static keyFunc_moveWordLeft() {
          SendInput "^{Left}"
          Return
      }

      static keyFunc_moveWordRight() {
          SendInput "^{Right}"
          Return
      }

      static keyFunc_backspace() {
          SendInput "{backspace}"
          Return
      }

      static keyFunc_delete() {
          SendInput "{delete}"
          Return
      }

      static keyFunc_deleteAll() {
          SendInput "^{a}{delete}"
          Return
      }

      static keyFunc_deleteWord() {
          SendInput "^+{left}"
          SendInput "{delete}"
          Return
      }

      static keyFunc_forwardDeleteWord() {
          SendInput "^+{right}"
          SendInput "{delete}"
          Return
      }

      static keyFunc_end() {
          SendInput "{End}"
          Return
      }

      static keyFunc_home() {
          SendInput "{Home}"
          Return
      }

      static keyFunc_moveToPageBeginning() {
          SendInput "^{Home}"
          Return
      }

      static keyFunc_moveToPageEnd() {
          SendInput "^{End}"
          Return
      }

      static keyFunc_deleteLine() {
          SendInput "{End}+{home}{bs}"
          Return
      }

      static keyFunc_deleteToLineBeginning() {
          SendInput "+{Home}{bs}"
          Return
      }

      static keyFunc_deleteToLineEnd() {
          SendInput "+{End}{bs}"
          Return
      }

      static keyFunc_deleteToPageBeginning() {
          SendInput "^+{Home}{bs}"
          Return
      }

      static keyFunc_deleteToPageEnd() {
          SendInput "^+{End}{bs}"
          Return
      }

      static keyFunc_enterWherever() {
          SendInput "{End}{Enter}"
          Return
      }

      static keyFunc_esc() {
          SendInput "{Esc}"
          Return
      }

      static keyFunc_enter() {
          SendInput "{Enter}"
          Return
      }

      static keyFunc_pageUp() {
          SendInput "{PgUp}"
          return
      }

      static keyFunc_pageDown() {
          SendInput "{PgDn}"
          Return
      }

      ;页面向上移动一页，光标不动
      static keyFunc_pageMoveUp() {
          SendInput "^{PgUp}"
          return
      }

      ;页面向下移动一页，光标不动
      static keyFunc_pageMoveDown() {
          SendInput "^{PgDn}"
          return
      }

      static keyFunc_undoRedo() {
          global
          if (ctrlZ)
          {
              SendInput "^{z}"
              ctrlZ := ""
          } Else
          {
              SendInput "^{y}"
              ctrlZ := 1
          }
          Return
      }

      static keyFunc_tabPrve() {
          SendInput "^+{tab}"
          return
      }

      static keyFunc_tabNext() {
          SendInput "^{tab}"
          return
      }

      static keyFunc_jumpPageTop() {
          SendInput "^{Home}"
          return
      }

      static keyFunc_jumpPageBottom() {
          SendInput "^{End}"
          return
      }

      static keyFunc_selectUp(i := 1) {
          SendInput "+{Up i}"
          return
      }

      static keyFunc_selectDown(i := 1) {
          SendInput "+{Down i}"
          return
      }

      static keyFunc_selectLeft(i := 1) {
          SendInput "+{Left i}"
          return
      }

      static keyFunc_selectRight(i := 1) {
          SendInput "+{Right i}"
          return
      }

      static keyFunc_selectHome() {
          SendInput "+{Home}"
          return
      }

      static keyFunc_selectEnd() {
          SendInput "+{End}"
          return
      }

      static keyFunc_selectToPageBeginning() {
          SendInput "^+{Home}"
          return
      }

      static keyFunc_selectToPageEnd() {
          SendInput "^+{End}"
          return
      }

      static keyFunc_selectCurrentWord() {
          SendInput "^{Left}"
          SendInput "^+{Right}"
          return
      }

      static keyFunc_selectCurrentLine() {
          SendInput "{Home}"
          SendInput "+{End}"
          return
      }

      static keyFunc_selectWordLeft(i := 1) {
          SendInput "^+{Left i}"
          return
      }

      static keyFunc_selectWordRight(i := 1) {
          SendInput "^+{Right i}"
          return
      }

      ;页面移动一行，光标不动
      static keyFunc_pageMoveLineUp(i := 1) {
          SendInput "^{Up i}"
          return
      }

      static keyFunc_pageMoveLineDown(i := 1) {
          SendInput "^{Down i}"
          return
      }

      static keyFunc_openCpasDocs() {
          Run "https://capslox.com/capslock-plus"
          return
      }

      static keyFunc_mediaPrev() {
          SendInput "{Media_Prev}"
          return
      }

      static keyFunc_mediaNext() {
          SendInput "{Media_Next}"
          return
      }

      static keyFunc_mediaPlayPause() {
          SendInput "{Media_Play_Pause}"
          return
      }

      static keyFunc_volumeUp() {
          SendInput "{Volume_Up}"
          return
      }

      static keyFunc_volumeDown() {
          SendInput "{Volume_Down}"
          return
      }

      static keyFunc_volumeMute() {
          SendInput "{Volume_Mute}"
          return
      }

      static keyFunc_reload() {
          MsgBox reload, 0.5
          Reload
          return
      }

      ; static keyFunc_send_dot(){
      ;     if(!static keyFunc_qbar_lowerFolderPath())
      ;         SendInput {U+002e}
      ;     return
      ; }

      static keyFunc_goCjkPage() {
          global
          Run "http://cjkis.me"
          return
      }

      ;keys functions end-------------
  }