#ErrorStdOut 'UTF-8'
SendMode "Input"
; 当前脚本运行的绝对目录
SetWorkingDir(A_ScriptDir)

;跳过对话框并自动替换旧实例, 效果类似于 Reload 函数.
#SingleInstance Force

global CLversion := "Version: 1.0 | by frank"

#Include %A_ScriptDir%
#Include lib
#Include init.ahk
#Include lib_keyFunctions.ahk
#Include lib_functions.ahk
#Include lib_keysSet.ahk
#Include lib_otherkeySet.ahk

A_MaxHotkeysPerInterval := 500
ProcessSetPriority("High")
global ctrlZ, CapsLock2, CapsLock

CapsLock::
{
    ;ctrlZ:     Capslock+Z undo / redo flag
    ;Capslock:  Capslock 键状态标记 i，按下是1，松开是0
    ;Capslock2: 是否使用过 Capslock+ 功能标记，使用过会清除这个变量
    ctrlZ := CapsLock2 := CapsLock := 1

    SetTimer(setCapsLock2, -100)

    KeyWait("CapsLock")
    CapsLock := ""	;Capslock最优先置空，来关闭Capslock+功能的触发
    if CapsLock2
        runFunc(keyset_press_caps)
    CapsLock2 := ""
    return

    setCapsLock2()
    {
        CapsLock2 := ""
        return
    }
}

#HotIf GetKeyState("CapsLock", "P")
;#region
a::
b::
c::
d::
e::
f::
g::
h::
i::
j::
k::
l::
n::
m::
o::
p::
q::
r::
s::
t::
u::
v::
w::
x::
y::
z::
1::
2::
3::
4::
5::
6::
7::
8::
9::
0::
f1::
f2::
f3::
f4::
f5::
f6::
f7::
f8::
f9::
f10::
f11::
f12::
space::
tab::
enter::
esc::
backspace::
ralt::
{
    str := %("keyset_caps_" A_ThisHotkey)%
    runFunc(str)
    CapsLock2 := ""
    return
}

`;::
{
    keyset_funcitons.%keyset_caps_semicolon%()
    CapsLock2 := ""
    return
}

,::
{
    keyset_funcitons.%keyset_caps_comma%()
    CapsLock2 := ""
    return
}

#HotIf 
;#endregion

!F5::
{
    Suspend(true)
}